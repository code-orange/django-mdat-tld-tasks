import requests
from cairosvg import svg2png
from celery import shared_task
from django.conf import settings

from django_mdat_customer.django_mdat_customer.models import MdatItems
from django_mdat_prod_info_mgmt.django_mdat_prod_info_mgmt.models import (
    MdatItemPictures,
)
from django_mdat_tld.django_mdat_tld.models import *


@shared_task(name="mdat_tld_sync_from_tld_list_com")
def mdat_tld_sync_from_tld_list_com():
    tld_request = requests.get("https://tld-list.com/df/tld-list-details.json")
    tld_data = tld_request.json()

    for tld, tld_info in tld_data.items():
        try:
            tld_type = MdatTopLevelDomainTypes.objects.get(name=tld_info["type"])
        except MdatTopLevelDomainTypes.DoesNotExist:
            tld_type = MdatTopLevelDomainTypes(
                name=tld_info["type"],
            )
            tld_type.save()

        try:
            mdat_tld = MdatTopLevelDomains.objects.get(name=tld)
        except MdatTopLevelDomains.DoesNotExist:
            mdat_tld = MdatTopLevelDomains(
                name=tld,
                type=tld_type,
            )
            mdat_tld.save()

        mdat_tld.type = tld_type
        mdat_tld.punycode = tld_info["punycode"]
        mdat_tld.right_to_left = tld_info["rtl"]
        mdat_tld.sponsor = tld_info["sponsor"]

        mdat_tld.save()

    return


@shared_task(name="mdat_tld_picture_sync")
def mdat_tld_picture_sync():
    picture_source = requests.get(
        "https://fonts.gstatic.com/s/i/short-term/release/materialsymbolsoutlined/public/default/48px.svg"
    )

    picture = svg2png(picture_source.content)

    items = (
        MdatItems.objects.filter(
            created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
        )
        .filter(
            external_id__startswith="HSDOM",
        )
        .filter(
            mdatitempictures__isnull=True,
        )
    )

    for item in items:
        new_picture = MdatItemPictures.objects.create(
            item=item,
            content=picture,
        )

    return
