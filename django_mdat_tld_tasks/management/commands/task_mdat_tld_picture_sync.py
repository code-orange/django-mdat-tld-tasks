from django.core.management.base import BaseCommand

from django_mdat_tld_tasks.django_mdat_tld_tasks.tasks import mdat_tld_picture_sync


class Command(BaseCommand):
    def handle(self, *args, **options):
        mdat_tld_picture_sync()
