from django.core.management.base import BaseCommand

from django_mdat_tld_tasks.django_mdat_tld_tasks.tasks import (
    mdat_tld_sync_from_tld_list_com,
)


class Command(BaseCommand):
    help = "Run task mdat_tld_sync_from_tld_list_com"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        mdat_tld_sync_from_tld_list_com()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
